classdef synscan < Telescope
% SYNSCAN A Matlab class to control a SkyWatcher mount.
%
%   S=SYNSCAN('IP:PORT')    Connects and initializes mount at IP:PORT
%
%   S=SYNSCAN('/dev/ttyS0') Connects and initializes mount at given
%   serial port (via cable).
%
%   S=SYNSCAN               Connects and initializes mount at 192.168.4.1:11880 (default).
%
% (c) E. Farhi 2024 - GPL3 

  properties
    status='INIT';
    axis  ={};
  end
  
  properties(SetAccess=protected)
    comm=[];  % the communication socket
    AZ  =1;
    ALT =2;
    RA  =3; % pseudo axis
    DEC =4; % pseudo axis
  end
  
  properties (Access=protected)
    commands       = []; % The list of commands (send/recv)
  end
  
  % ----------------------------------------------------------------------------
  
  methods
  
    function s = synscan(varargin)
    % SYNSCAN The SynScan constructor, that initializes the connection.
    %
    %   S=SYNSCAN('IP:PORT') Connects and initializes mount at IP:PORT
    %
    %   S=SYNSCAN('/dev/ttyS0') Connects and initializes mount at given
    %   serial port (via cable).
    %
    %   S=SYNSCAN Connects and initializes mount at 192.168.4.1:11880 (default).
    %
    %   The SYNSCAN_UDP_IP and SYNSCAN_UDP_PORT environment variables can be
    %   defined to specify a Wifi connection.
    
      if nargin == 0
        ip=getenv('SYNSCAN_UDP_IP');
        if ~isempty(ip) && ~isempty(getenv('SYNSCAN_UDP_IP'))
          ip = [ ip ':' getenv('SYNSCAN_UDP_PORT') ];
        end
        if ~isempty(ip)
          varargin{1} = ip;
        end
      end
      s.comm           = socket(varargin{:});
      if isempty(s.comm) || isempty(s.comm.fileno)
        error([ mfilename ': Can not connect' ]);
      end
      s.comm.TimeOut   = 1;
      % remove socket/pnet warnings
      warning('off','pnet:fwrite');
      warning('off','pnet:fread');
      s.comm.Terminator= sprintf('\r');
      s.Device_Port    = char(s.comm, 'short');
      s.commands       = getAllCommands;
      
      % start communication
      start(s);
      if isfield(s.axis{s.AZ}, 'getMountType') t=s.axis{s.AZ}.getMountType;
      else t=mfilename; end
      disp([ 'Welcome ! You are using a ' t ' mount.' ]);
      disp([ mfilename ': The mount assumes to initially point to the North Horizon (AZ=0, ALT=0).' ]);

    end % synscan
    
    function out = sendrecv(self, cmd, ax, data, raw)
    % SENDRECV Send a single command for given axis and get result.
    %
    %   R=SENDRECV(S, CMD, AX, DATA) Sends command CMD with payload DATA to axis
    %   AX and returns result. The result is decoded. The DATA can be given as a 
    %   string (sent as is), or a number which is encoded as an 6-HEX string.
    %
    %   R=SENDRECV(S, CMD, AX, DATA, 'RAW') Sends command CMD with payload DATA 
    %   to axis AX and returns raw result.
    %
    %   R=SENDRECV(S, CMD, AX) Sends command CMD to axis AX and returns result.
    %
    %   R=SENDRECV(S, CMD) Sends command CMD to axis 1 and returns result.
    %
    %   Valid axes are e.g. 'az','alt', 1=S.AZ, 2=s.ALT

      if nargin < 3, ax     =[]; end
      if nargin < 4, data   =''; end
      if nargin < 5, raw    =false; end
      ax = axisalias(self, ax);
      if isnumeric(data), data=int2hex(data); end % also swaps bytes (6 digits).
      
      % resolve command aliases
      if numel(cmd) > 1 
        [c,f] = getcommand(self, cmd);
      else c=[]; end
      if ~isempty(c), cmd = c{1}; end
      if numel(cmd) > 1 || ~ischar(data)
        warning([ mfilename ': ERROR: Invalid command ' cmd ]);
        return
      elseif any(ax == [ self.RA self.DEC ])
        % this is a pseudo axis. No low level comm.
        warning([ mfilename ': Pseudo-axis ' num2str(ax) '. No low communication.' ]);
        out = [];
        return
      elseif ~any(ax == [ self.AZ self.ALT ])
        warning([ mfilename ': ERROR: Invalid axis ' num2str(ax) ]);
        return
      end
      
      if numel(ax) > 1
        out = {};
        for a=ax
          out{end+1} = sendrecv(self, cmd, a, data, raw);
        end
        return
      end
      
      % Sent data for 'set' commands is:
      %   DB1-6: 6 digits for most commands
      %   DB1-2: 2 digits for setMotionMode, setPolarScopeBrightness
      %   DB1:   1 digit  for setSleep,      setAuxSwitch, setAutoGuide
      
      msg = [ ':' char(cmd) num2str(ax) data self.comm.Terminator ];
      out  = self.comm.cmd(msg); % socket fwrite,fread
      
      % interpret result
      if nargin==5, return; end % 'raw'
      if out(1) == '='      % normal response, OK
        out = out(2:end-1); % remove '=' and Terminator '\r'
        out = hex2int(out); % handle D(2),E(3), and B(6) digits output with byte swapping.
 
      elseif out(1) == '!'  % error
        out = hex2dec(out(2:end-1)); % remove '!' and Terminator '\r'
        err = {'Unknown Command',   'Command Length Error',    'Motor Not Stopped', ...
               'Invalid Character', 'Not Initialized',         'Driver Sleeping', ...
               'Unknown Error',     'PEC Training Is Running', 'No Valid PEC Data'};
        if out>=0 && out< numel(err) 
          out = err{out+1}; % error code starts at 0-8
        else
          out = [ num2str(out) ':Unknown Error' ];
        end
        warning([ mfilename ': CMD=' msg(1:(end-1)) ' Error=' out ])
        out = [];
      end
    end % sendrecv
    
    function delete(self) % also closes
    % DELETE Close the current scope connection.
      fclose(self.comm);
      self.comm=[];
    end % delete
    
    function angle = gotoaxis(self, ax, angle)
      % GOTOAXIS Send a single axis to given angle.
      %
      %   GOTOAXIS(S, AX, ANGLE) Slews given mount axis AX to ANGLE (in degrees).
      %
      %   A=GOTOAXIS(S, AX)      Returns current axis angle (in degrees).
      
      
      if nargin < 2 || isempty(angle)
        angle = [];
        return
      end
      ax = axisalias(self, ax);
      
      if numel(ax) > 1
        out = [];
        for a=ax
          out(end+1) = gotoaxis(self, a, angle);
        end
        angle = out;
        return
      end
      
      if all(ax ~= [self.AZ self.ALT])
        warning([ mfilename ': gotoaxis: invalid axis ' num2str(ax) ]);
        return
      end

      % stop mount if moving
      stop(self, ax);
      
      % set goto mode
      if ~isfield(self.axis{ax}, 'getPositionDegrees')
        warning([ mfilename ': Can not get Axis ' num2str(ax) ' PositionDegrees.' ]);
        return
      end
      getstatus(self,'getPosition'); % read axes angles
      actualPos = self.axis{ax}.getPositionDegrees;
      if nargin < 3
        angle = actualPos;
        return
      end
      sendrecv(self, 'setMotionMode', ...
        ax, setMotionMode('goto', 'high', angle > actualPos) );
      
      % set target (in counts)
      if ~isfield(self.axis{ax}, 'getCountsPerRevolution') ...
      || ~self.axis{ax}.getCountsPerRevolution
        warning([ mfilename ': Can not get Axis ' num2str(ax) ' CountsPerRevolution.' ]);
        return
      end
      counts = round(angle * self.axis{ax}.getCountsPerRevolution / 360);
      sendrecv(self, 'setGotoTargetPosition', ax, counts+hex2dec('800000') );
      self.axis{ax}.getGotoTargetPosition=counts+hex2dec('800000');
      self.axis{ax}.getGotoTargetPositionDegrees = angle;
      
      % start motion
      sendrecv(self, 'startMotion', ax);
    end % gotoaxis
    
    function g = goto(self, varargin)
    % GOTO Send SynScan mount to given RA/Dec coordinates.
    %
    %   GOTO(S, RA,DEC) Sends a GOTO request for EQ coordinates.
    %   GOTO(S, struct('ra',RA,'dec',DEC)) Sends a GOTO request for EQ coordinates.
    %   The RA  can be given in hours   or as a string HH:MM:SS
    %   The DEC can be given in degrees or as a string DD°MM'SS" or DD*MM:SS.
    %
    %   GOTO(S, struct('alt',ALT,'az',AZ)) Sends a GOTO request for Alt-Az coordinates.
    %
    %   GOTO(S, 'M 51') Sends a GOTO request for a named object.
      
      ra=[]; dec=[]; alt=[]; az=[]; name=[]; g= [];
      
      % handle input arguments
      if nargin == 1
        g = self.Target_Coord;
        return
      elseif nargin <= 2 && isnumeric(varargin{1})
        ra = varargin{1};
      elseif nargin == 2 && ischar(varargin{1})
        varargin{1} = findobj(self, varargin{1});
      elseif nargin <= 3 && isnumeric(varargin{1}) && isnumeric(varargin{2})
        ra  = varargin{1};
        dec = varargin{2};
      end
      if isstruct(varargin{1})
        c = varargin{1};
        if isfield(c, 'ra'),  ra =c.ra;  end
        if isfield(c, 'dec'), dec=c.dec; end
        if isfield(c, 'RA'),  ra =c.RA;  end
        if isfield(c, 'DEC'), dec=c.DEC; end
        if isfield(c, 'alt'), alt=c.alt; end
        if isfield(c, 'az'),  az =c.az;  end
        if isfield(c, 'NAME'),  name =c.NAME;  end
        if isfield(c, 'name'),  name =c.name;  end
      end
      if isempty([az alt ra dec ])
        n = numel(varargin);
        for i=1:n
          if     strcmpi(varargin{i}, 'ra' ) && i < n, ra =varargin{i+1}; i=i+1;
          elseif strcmpi(varargin{i}, 'dec') && i < n, dec=varargin{i+1}; i=i+1;
          elseif strcmpi(varargin{i}, 'az')  && i < n, az =varargin{i+1}; i=i+1;
          elseif strcmpi(varargin{i}, 'alt') && i < n, alt=varargin{i+1}; i=i+1;
          end
        end
      end
      if isempty([az alt ra dec ])
        warning([ mfilename ': goto: no target given.' ]);
        return
      end
    
      % goto ra/dec
      self.Target_Coord = [];
      if ~isempty(ra) && ~isempty(dec) && isempty([ az alt])
        [az, alt]= radec2azalt(self, ra,dec);
        self.Target_Coord.ra  = ra;
        self.Target_Coord.dec = dec;
      end
      
      % goto alt/az
      if ~isempty(alt), 
        self.Target_Coord.alt  = gotoaxis(self, self.ALT, alt);
      end
      if ~isempty(az), 
        self.Target_Coord.az   = gotoaxis(self, self.AZ,  az);
      end
      if ~isempty(name)
        self.Target_Coord.name = name;
      end
      g = self.Target_Coord;
    end % goto
    
    function home(self, arg);
    % HOME Move mount to its home position, e.g. all angles at 0.
    %
    %   HOME(S, 'set') Sets current position as the HOME.
    
      % e.g. goto North Horizon
      if nargin < 2, arg=''; end
      if strcmpi(arg, 'set')
        sync(self, [ self.AZ self.ALT ]);
      else
        gotoaxis(self, [self.ALT self.AZ], 0);
      end
    end % home
    
    function degreesPerSecond=speed(self, ax, degreesPerSecond)
    % SPEED Set the slew speed in degrees per second.
    %
    %   SPEED(S, AX, deg_per_sec) Set mount axis AX speed to degrees per second.
    %
    %   SPEED(S, AX, 'level')     Set mount axis AX speed to given speed level (1:max).
    %
    %   SPEED(S, [], deg_per_sec) Set all mount axes speed to degrees per second.
    %
    %   S=SPEED(S) Returns the current slew speed (S.Telescope_Slew_Rate).
    %
    %   The speed can be given as well as 
    %     '1','sidereal'    0.0042 deg/s
    %     '2','center'      0.04   deg/s
    %     '3','find'        0.4    deg/s
    %     '4','max'         4      deg/s
    %     'increase','up'   incrase  the speed
    %     'decrease','down' decrease the speed
    
      if nargin  == 1,
        degreesPerSecond = self.Telescope_Slew_Rate;
        return
      end
    
      if nargin == 2 && ischar(ax), degreesPerSecond=ax; ax=[]; end
      ax = axisalias(self, ax);
    
      degreesPerSecond = speedalias(self, degreesPerSecond); % in Telescope
      
      if numel(ax) > 1
        out = [];
        for a=ax
          out(end+1) = speed(self, a, degreesPerSecond);
        end
        degreesPerSecond = out;
        return
      elseif isempty(ax)
        return
      end

      self.Telescope_Slew_Rate = degreesPerSecond;
      if degreesPerSecond==0, stop(self, ax); return; end
      
      if ~isfield(self.axis{ax}, 'getCountsPerRevolution') ...
      || ~isfield(self.axis{ax}, 'getTimerInterruptFreq') ...
      || ~self.axis{ax}.getCountsPerRevolution ...
      || ~self.axis{ax}.getTimerInterruptFreq
        warning([ mfilename ': Can not get Axis ' num2str(ax) ' CountsPerRevolution.' ]);
        return
      end
      
      countsPerSecond = abs(degreesPerSecond) * self.axis{ax}.getCountsPerRevolution / 360;
      % set pulse frequency
      data = round(self.axis{ax}.getTimerInterruptFreq / countsPerSecond);
      sendrecv(self, 'setStepPeriod', ax, data );
      self.axis{ax}.getStepPeriod = data;

    end % speed
    
    function s=start(self)
    % START Initialize, and get full mount settings.
    
      fopen(self.comm); self.status='CONNECTED';
      s=getstatus(self, 'full');
      speed(self, 'find');
      getstatus(self, 'getStepPeriod');
      disp([ mfilename ': Starting ' char(self) ]);
    end % start
    
    function c=stop(self, ax, hard) % stop current move
    % STOP Stop the mount move.
    %
    %   STOP(S) Stops both mount axes. Assumes AX=[self.ALT self.AZ]
    %
    %   STOP(S, AX) Stops mount axis (1,2 or [self.ALT self.AZ]).
    %
    %   STOP(S, 'hard')     Stops the mount immediately.
    %   STOP(S, AX, 'hard') Stops the mount axis immediately.
    
      if nargin < 2,  ax=[];  end
      if strcmp(ax, 'hard'), ax=[]; hard=true;
      elseif nargin < 3,  hard=false; end
      
      if ~hard, c = 'AxisStop';
      else      c = 'InstantStop'; 
      end
      
      ax = axisalias(self, ax, [self.AZ self.ALT]);
      % send the command
      for a=ax
        sendrecv(self, c, a);
      end
    end % stop
    
    function sync(self, ax, varargin) % Set axis offset.
    % SYNC Assigns position to axis (offset) useful to align/sync scope.
    %
    %   SYNC(S) Assumes target is reached, sets all offsets accordingly.
    %
    %   SYNC(S, AX) Assumes target is reached for given axis, sets offsets.
    %
    %   SYNC(S, AX, angle) Sets AX offset so that angle is current.
    
      if nargin == 1, ax=[]; end
      ax = axisalias(self, ax, [self.AZ self.ALT]);
      
      if numel(ax) > 1
        for a=ax
          sync(self, a, varargin{:});
          return
        end
      end
      
      if nargin < 3 ...
      && isfield(self.axis{ax}, 'getGotoTargetPositionDegrees')
        angle = self.axis{ax}.getGotoTargetPositionDegrees;
      else 
        warning([ mfilename ': sync: Invalid angle offset for axis ' num2str(ax) ]);
        return
      end
    
      counts = round(angle * self.axis{ax}.getCountsPerRevolution / 360);
      sendrecv(self, 'setPosition', ax, counts+hex2dec('800000') );
      getstatus(self);
    end % sync
    
    function degreesPerSecond=track(self, ax, degreesPerSecond)
    % TRACK Slew/track/move a single axis with given speed.
    %
    %   TRACK(S, AX, SPEED) Slew/track/move given mount axis AX with SPEED degrees per second.
    %   The sign of SPEED determines the rotation direction.
    %   Requires STOP(S, AX) to stop movement.
    %
    %   TRACK(S, S.AZ, 'sidereal') Tracks horizontal axis (Azimuth) at sidereal
    %   rate. This is suited when the mount is in Equatorial settings.
    %
    %   The speed can be given as well as 'sidereal','center','find','max' for
    %   [0.0042 0.01 0.4 0.4] deg/s, as well as 'up' and 'down'.
    
      if nargin < 2, ax=[]; end
      [ax,s] = axisalias(self, ax); % also get sign
      if nargin < 3, degreesPerSecond=''; end
      if isempty(degreesPerSecond)
        degreesPerSecond = s*self.Telescope_Slew_Rate;
      end
      
      if numel(ax) > 1
        out = [];
        for a=ax
          out(end+1) = track(self, a, degreesPerSecond);
          return
        end
      end
      
      stop(self, ax); % axis must be stopped.
      
      if isempty(degreesPerSecond) || (isnumeric(degreesPerSecond) && degreesPerSecond==0)
        return
      end
      
      % set tracking mode and speed
      degreesPerSecond = speed(self, ax, degreesPerSecond);
      sendrecv(self, 'setMotionMode', ax, setMotionMode('track', 'low', degreesPerSecond) );
      
      % start motion
      sendrecv(self, 'startMotion',   ax);
    
    end % track

  end


% ------------------------------------------------------------------------------
% list of command aliases

  methods (Access=private)
    
    function [c,f] = getcommand(self, cmd)
    % GETCOMMAND Get a list of commands that match a pattern.
      allcmds = self.commands;
      cmd     = cellstr(cmd);
      c       = {};
      f       = {};
      for i=1:numel(cmd)
        thisc = cmd{i}; m=[];
        if numel(cmd{i}) > 2  % the command is not a single char
          if thisc(end) == '*'      % wildcard e.g. for 'get*'
            m = find( strncmp(allcmds(:,1), thisc, numel(thisc(1:(end-1)))) );
          else
            m = find( strcmpi( allcmds(:,1), cmd{i}) );
          end
        elseif numel(cmd{i}) == 1    % a single cmd, get its alias
          m = find( strcmp(allcmds(:,2), cmd{i}) );
        end
        if ~isempty(m)
          f = { f{:} allcmds{m,1} }; % command full name (alias)
          c = { c{:} allcmds{m,2} }; % command key
        else
          c = { c{:} cmd{i} }; f={ f{:} [ 'command_' num2str(i) ] }; % not found
        end
      end
    end
    
    function [ax, s] = axisalias(self, ax, default)
    % AXISALIAS Resolves aliases for axes.
    %
    %   [ax, sign]=AXISALIAS(S, ax) Retunrs resolved 'ax' and optionally its 'sign'.
    
      if nargin < 3, default=self.AZ; end
      s=1;
      if ischar(ax)
        switch lower(ax)
        case {'alt','n','alt+','north'}
          ax = self.ALT; 
        case {'alt-','s','south'}
          ax = self.ALT; s=-1;
        case {'az','e','az+','east'}
          ax = self.AZ; 
        case {'az-','w','west'}
          ax = self.AZ; s=-1;
        case {'ra','ra+'}
          ax = self.RA; 
        case {'ra-'}
          ax = self.RA; s=-1;
        case {'dec','dec+'}
          ax = self.DEC; 
        case {'dec-'}
          ax = self.DEC; s=-1;
        case {'all'}
          ax = [self.AZ self.ALT self.RA self.DEC ];
        case {'real','physical'}
          ax = [self.AZ self.ALT ];
        case {'pseudo'}
          ax = [self.RA self.DEC ];
        otherwise
          ax = '';
        end
      elseif isnumeric(ax)
        if any(ax < 0 | ax > 4)
          error([ mfilename ': invalid axis ID.' num2str(ax)]);
        end
      end
      if isempty(ax), ax=default; end
    end % axisalias
    
  end
  
end % class

