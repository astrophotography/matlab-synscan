function mode = setMotionMode(Type, Silent, Speed)
  % setMotionMode Encode the type of movement
  %
  %   setMotionMode(Type, Silent, Speed)
  %   Type:   'goto' or 'slew'/'track'/'move'
  %   Silent: 'silent'/'slow','low' or 'fast'/'high'
  %           low speed mode can be used to save battery, or when near target.
  %   Speed:  negative for CCW, positive for CW
  
  % Func - 0 High speed slew to mode (goto)
  % Func - 2 Low  speed slew to mode (goto)
  % Func - 1 Low  speed slew mode    (slew/track/move)
  % Func - 3 High speed slew mode    (slew/track/move)
  
  % Direction - 0 for speed < 0 (reduce   angle, CW)
  % Direction - 1 for speed > 0 (increase angle, CCW)
  
    if strcmpi(Type,'goto')
      if any(strcmpi(Silent,{'silent','slow','low'}))
        Func = '2'; % slow
      else
        Func = '0'; % fast
      end
    else % Type = slew/move/track
      if any(strcmpi(Silent,{'silent','slow','low'}))
        Func = '1'; % slow
      else
        Func = '3'; % fast
      end
    end
  
    % slew/goto
    if (Speed >= 0.0), Direction = '0';
    else               Direction = '1'; 
    end
      
    mode = [ Func Direction ];
  end
