function data = int2hex(data,ndigits)
% INT2HEX Convert an integer to an hexadecimal string with given digit.

%  * 24 bits Data Sample: for HEX number 0x123456, in the data segment of
%    a command or response, it is sent/received in this order: "5" "6" "3" "4" "1" "2".
%  * 16 bits Data Sample: For HEX number 0x1234, in the data segment of a command or 
%    response, it is sent/received in this order: "3" "4" "1" "2". 
%  * 8 bits Data Sample: For HEX number 0x12, in the data segment of a command or
%    response, it is sent/received in this order: "1" "2".
  
  if isempty(data), return; end
  if nargin < 2, ndigits=6; end

  if ~ischar(data)
    data = dec2hex(data,ndigits); % synscan mostly uses 6-digit encoding
  end
  n = numel(data);
  if any(n == [1 3 5]), return; end
  if any(n == 1:6)
    order = [5 6 3 4 1 2];
    order = order((end-n+1):end);
    data = data(order);
  end
  
end
