function c = getAllCommands
% GETCOMMANDS Get all known commands.
  
  c = { ...
      'AxisStop',               'K';
      'InstantStop'             'L';
      'getAuxEncoder',          'd';
      'getBreakPoint',          'm';
      'getBreakSteps',          'c';
      'getCountsPerRevolution', 'a';
      'getGotoTargetPosition',  'h';
      'getHighSpeedRatio',      'g';
      'getMotorBoardVersion',   'e';
      'getPECPeriod',           's';
      'getPosition',            'j';
      'getStatus',              'f';
      'getStepPeriod',          'i';
      'getTimerInterruptFreq',  'b';
      'getTrackingPeriod',      'D';
      'Initialize',             'F';
      'setMotionMode',          'G';
      'setGotoTargetPosition',  'S';
      'setPosition',            'E';
      'setStepPeriod',          'I';
      'startMotion',            'J';
     };
     
end
