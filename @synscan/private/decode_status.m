function [status, s] = decode_status(hexstring)
% DECODE_STATUS Decode Status msg.
%
% Status msg is 12 bits long (3 HEX digits).
%  HEX digit1 bits:
%
%  * B0: 1=Tracking,        0=Goto
%  * B1: 1=CCW,             0=CW
%  * B2: 1=High Speed,      0=Low Speed
%
%  HEX digit2 bits:
%
%  * B0: 1=Running,         0=Stopped
%  * B1: 1=Blocked,         0=Normal
%
%  HEX digit3 bits:
%
%  * B0: 1=Init done,       0=Not Init(starting)
%  * B1: 1=Level switch on, 0=OFF
  
  
  % we convert back to HEX string, extract bytes, then bits.
  if ~ischar(hexstring)
    hexstring = int2hex(hexstring);
  end
  if numel(hexstring) ~= 3
    warning([ mfilename ': ERROR reading status message "' hexstring '"' ]);
    status=[]; 
    return; 
  end
  
  A = hex2dec(hexstring(1));
  B = hex2dec(hexstring(2));
  C = hex2dec(hexstring(3));
  
  % extract the relevant bits
  s = [ logical(bitget(A, 1)) logical(bitget(A, 2)) logical(bitget(A, 3)) ...
        logical(bitget(B, 1)) logical(bitget(B, 2)) ...
        logical(bitget(C, 1)) logical(bitget(C, 2)) ];
  status = struct(...
      'Tracking',       s(1),...
      'CCW',            s(2),...
      'FastSpeed',      s(3),...
      'Running',        s(4),...
      'Blocked',        s(5),...
      'InitDone',       s(6),...
      'LevelSwitchOn',  s(7)...
  );
    
end
