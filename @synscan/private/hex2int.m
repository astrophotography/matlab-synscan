function data = hex2int(data)
% INT2HEX Convert an hexadecimal string with given digit to an integer.

  %  * 24 bits Data Sample: for HEX number 0x123456, in the data segment of
  %    a command or response, it is sent/received in this order: "5" "6" "3" "4" "1" "2".
  %  * 16 bits Data Sample: For HEX number 0x1234, in the data segment of a command or 
  %    response, it is sent/received in this order: "3" "4" "1" "2". 
  %  * 8 bits Data Sample: For HEX number 0x12, in the data segment of a command or
  %    response, it is sent/received in this order: "1" "2".

  % Returned message types for 'get/inquire' commands are:
  %   B: 6 chars
  %   D: 2 chars     for getHighSpeedRatio
  %   E: 3 chars     for getStatus

  v = [];
  if isempty(data), return; end
  n = numel(data);
  
  if     n == 3, 
    return; % -> decode_status      E(3): status
  elseif n == 2, 
    data  = hex2dec(data);      % D(2): HighSpeedRatio
  elseif any(n==[4 6])            % B(6): usual value, swap bytes
    order = [5 6 3 4 1 2];
    order = order((end-n+1):end);
    data  = hex2dec(data(order));       
  end
  
end
