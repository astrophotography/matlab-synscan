function s=getstatus(self, arg)
% GETSTATUS Get the scope status, e.g. coordinates.
%
%   ST=GETSTATUS(S) Gets the current position and return the mount state
%   which can be GOTO/MOVING (when moving) or TRACKING/IDLE.
%
%   ST=GETSTATUS(S, 'full') Gets full mount state, as done when starting.
%
%   AX=GETSTATUS(S, 'axis') Gets current mount angles as a struct.

  if nargin < 2, arg=''; end
  if any(strcmp(self.status, {'CONNECTED','INIT'})) ...
  || any(strcmp(arg, {'full','force','init'}))
    cmd = {'get*','Initialize'};
    self.axis{self.AZ }.name='Azimuth (horizontal)';
    self.axis{self.ALT}.name='Altitude (vertical)';
    self.axis{self.RA }.name='Right Ascension';
    self.axis{self.DEC}.name='Declinaison';
    arg = '';
  elseif isempty(arg) || any(strcmp(arg, {'axis','axes','getPosition','angles'}))
    cmd = {'getPosition','getStatus'};
  else
    cmd = arg;
  end
  
  [cmd,f] = getcommand(self, cmd); % resolve aliases
  isStopped= 1;
  isMoving = 0;
  isGoto   = 0;
  isInit   = 1;
  cmd = cellstr(cmd);
  for i=1:numel(cmd)
    for ax=[ self.ALT self.AZ ]
      if any(ax == [ self.ALT self.AZ ])
        out = sendrecv(self, cmd{i}, ax);
      else
        out = [];
      end
      if isempty(out), continue; end
      
      % decode result for special commands
      if strcmpi(f{i}, 'getStatus')
        [decoded,bits] = decode_status(out); % return a struct
        if isempty(decoded), continue; end
        self.axis{ax}.status       = decoded;
        self.axis{ax}.getStatusBits= bits;
        if ~decoded.Blocked
          if  decoded.Running,  isMoving  = 1; end
          if ~decoded.Tracking, isGoto    = 1; end % GOTO/SLEW
          if ~decoded.InitDone, isInit    = 0; end
        end
        
      elseif any(strcmpi(f{i}, {'getPosition','getGotoTargetPosition'})) ...
        && isfield(self.axis{ax}, 'getCountsPerRevolution') ...
        && self.axis{ax}.getCountsPerRevolution
        % the getGotoTargetPosition may remain at 0 in all cases. we use stored value.
        if strcmpi(f{i}, 'getGotoTargetPosition') && ~out ...
        && isfield(self.axis{ax}, 'getGotoTargetPosition') ...
        && self.axis{ax}.getGotoTargetPosition
          out = self.axis{ax}.getGotoTargetPosition;
        end
        out = out - hex2dec('800000');
        self.axis{ax}.([ f{i} 'Degrees' ]) = out * 360 / self.axis{ax}.getCountsPerRevolution;
        
      elseif strcmpi(f{i}, 'getStepPeriod') ...
        && isfield(self.axis{ax}, 'getCountsPerRevolution') ...
        && isfield(self.axis{ax}, 'getTimerInterruptFreq') ...
        && self.axis{ax}.getCountsPerRevolution
        
        % the getStepPeriod may remain at 0 in all cases. we use stored value.
        if ~out && isfield(self.axis{ax}, 'getStepPeriod') && self.axis{ax}.getStepPeriod
          out = self.axis{ax}.getStepPeriod;
        end
        % set pulse frequency
        countsPerSecond = self.axis{ax}.getTimerInterruptFreq / out;
        self.axis{ax}.getStepPeriodDegrees = countsPerSecond/self.axis{ax}.getCountsPerRevolution*360;
        
      elseif strcmpi(f{i}, 'getMotorBoardVersion')
        % The mount type is given by bytes FF0000
        MCVersion = bitshift(bitand(out, hex2dec('FF0000')), -16);
        mounts={'EQ6', hex2dec('00'); 'HEQ5', hex2dec('01'); 'EQ5', hex2dec('02'); 'EQ3', hex2dec('03'); 'EQ8', hex2dec('04'); 'AZEQ6', hex2dec('05'); 'AZEQ5', hex2dec('06'); 'STAR_ADVENTURER', hex2dec('0A'); 'EQ8R_PRO', hex2dec('20'); 'AZEQ6_PRO', hex2dec('22'); 'EQ6_PRO', hex2dec('23'); 'EQ5_PRO', hex2dec('31'); 'GT', hex2dec('80'); 'MF', hex2dec('81'); '_114GT', hex2dec('82'); 'DOB', hex2dec('90'); 'AZGTE', hex2dec('A2'); 'AZGTI', hex2dec('A5')};
        index=find(MCVersion == [ mounts{:,2} ]);
        if numel(index) == 1
          self.axis{ax}.getMountType = mounts{index,1};
        end
      end
      
      % store
      self.axis{ax}.(f{i}) = out;
    end % axis
  end % cmd
  
  % set the pseudo axes
  
  if ~isInit, 
    self.status='CONNECTED';
  elseif isGoto
    self.status='GOTO';
  elseif isMoving
    self.status='MOVING';
  elseif ~isempty(self.Telescope_Track_Rate)
    self.status='TRACKING';
  else
    self.status='IDLE';
  end

  if isfield(self.axis{self.AZ},'getPositionDegrees')
    self.Horizontal_Coord.az = self.axis{self.AZ}.getPositionDegrees;
  end
  if isfield(self.axis{self.ALT},'getPositionDegrees')
    self.Horizontal_Coord.alt = self.axis{self.ALT}.getPositionDegrees;
  end
  % get current RA/Dec
  if isfield(self.axis{self.AZ },'getPositionDegrees') ...
  && isfield(self.axis{self.ALT},'getPositionDegrees') ...
    [self.Equatorial_Coord.ra self.Equatorial_Coord.dec] = ...
      azalt2radec(self, self.Horizontal_Coord.az, self.Horizontal_Coord.alt);
    self.axis{self.RA }.getPosition        = self.Equatorial_Coord.ra;
    self.axis{self.RA }.getPositionDegrees = self.Equatorial_Coord.ra*15;
    self.axis{self.DEC}.getPosition        = self.Equatorial_Coord.dec;
    self.axis{self.DEC}.getPositionDegrees = self.Equatorial_Coord.dec;
  end
  
  % set return value
  if any(strcmpi(arg, {'axis','axes','getPosition','angles'}))
    s = struct('az', self.Horizontal_Coord.az, 'alt',self.Horizontal_Coord.alt, ...
               'ra', self.Equatorial_Coord.ra, 'dec',self.Equatorial_Coord.dec);
  elseif ~isempty(arg) && ischar(arg)
    s = out;
  else
    s = self.status;
  end
  
end % getstatus

