# matlab-synscan

A Matlab class to control a Skywatcher mount.

## Description

This is a class to be used with Matlab in order to control e.g. a [Skywatcher AZ-GTi](https://skywatcher.com/product/az-gti-mount/) lightweight astro-photography mount. It may also control other SW mounts, such as the EQ6 or the SAM.

:warning: This is a starting project, not functional yet...

## Setting-up the mount

### Azimuthal settings

Initially, in Azimuthal settings, the mount is assumed to be levelled, and its attached scope should be pointing the North horizon.

#### Azimuthal settings: automatic alignement

An automatic alignement procedure for Azimuthal settings could be:

- check the latitude,longitude (may use `getplace`).
- bring the scope to e.g. Az=0 Alt=85 (nearly vertical wrt its base).
- take picture and solve-plate (determine RA,Dec and Az,Alt coordinates from the image).
- bring Az to 180 (half turn).
- take picture and solve-plate (determine RA,Dec and Az,Alt coordinates from the image).
- compute the half-way Az,Alt from the 2 absolute coordinates.
- bring the scope vertical wrt its base, set Alt=90 (offset/zero). This defines the vertical axis of the mount.
- compute the direction to the North from the 2 absolute coordinates, set Az=0 (offset/zero). This defines the horizontal plane.
- bring the scope to Az=0,Alt=90
- take picture and solve-plate (determine RA,Dec from the image).
- refine the GPS location using `coords2gps`.

The mid-point computation can be done using `sph2cart` for both `[Az,Alt]` points, compute the half `[X,Y,Z]` location, and then `cart2sph` back to `[Az,Alt]`.

### Equatorial settings

In case the scope is in Equatorial settings, the 'Azimuth' mount axis (1) is mostly the Right Ascension (RA), and the 'Altitude' (Elevation) mount axis (2) is the Declinaison.
Initially, the mount should be mostly pointing to the Pole when Alt=90, and its Azimuth should be pointing to the West (i.e. connectors are on the left when looking at the Polar star). Then lower its Altitude to 0 (should point to the ground when in the North pole).

#### Equatorial settings: automatic alignement

An automatic alignement procedure for Equatorial settings could be:

- check the latitude,longitude (may use `getplace`).
- bring the scope to e.g. Alt=85 (nearly vertical wrt its base).
- take picture and solve-plate (determine RA,Dec and Az,Alt coordinates from the image).
- bring Az to 180 (half turn).
- take picture and solve-plate (determine RA,Dec and Az,Alt coordinates from the image).
- compute the half-way Az,Alt from the 2 absolute coordinates.
- bring the scope vertical wrt its base, set Alt=Dec=90 (offset/zero). This defines the vertical axis of the mount, which mostly points to the Polar (in the North pole).
- compute the direction to the North from the 2 absolute coordinates, set Az=0 (offset/zero). This defines the horizontal plane.
- bring the scope to Alt=Dec=90 (pole)
- take picture and solve-plate (determine RA,Dec from the image).
- refine the GPS location using `coords2gps`.

The mid-point computation can be done using `sph2cart` for both `[Az,Alt]` points, compute the half `[X,Y,Z]` location, and then `cart2sph` back to `[Az,Alt]`.


## Credits
(c) E. Farhi 2024 - GPL3

Documentation on the Skywatcher commands:

- https://inter-static.skywatcher.com/downloads/skywatcher_motor_controller_command_set.pdf
- https://github.com/skywatcher-pacific/skywatcher_open/wiki/Skywatcher-Protocol
- https://github.com/indilib/indi/blob/master/drivers/telescope/skywatcherAPI.h
- https://github.com/indilib/indi/blob/master/drivers/telescope/skywatcherAPI.cpp
- https://indilib.org/individuals/devices/telescopes/skywatcher/skywatcher-alt-az-dobsonian.html

